from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return render(request, 'index.html', {
        #context
        'title': 'Curso Django',
        'message': 'Productos',
        'products': [
            {'title': 'Camisa', 'price':50, 'stock':True},
            {'title': 'Pantalon', 'price':100, 'stock':False},
            {'title': 'Gafas', 'price':35, 'stock':True},
        ]
    })

def provincias(request):
    return render(request, 'provincias.html', {
        #context
        'title': 'Deber',
        'message': 'Provincias de la Sierra',
        'products': [
            {'title': 'Pichincha', 'danger':True},
            {'title': 'Carchi', 'danger':False},
            {'title': 'Tungurahua', 'danger':False},
            {'title': 'Chimborazo', 'danger':False},
            {'title': 'Cañar', 'danger':False},
            {'title': 'Azuay', 'danger':False},
            {'title': 'Loja', 'danger':False},
            {'title': 'Imbabura', 'danger':False},
            {'title': 'Bolivar', 'danger':False},
            {'title': 'Cotopaxi', 'danger':False},
        ]
    })