--------------------------------
-- crear un entorno de python --
--------------------------------
python -m venv envrdj


------------------------------------
-- componentes VSCode para python --
------------------------------------
djaneiro
Django


IDE PyCharm
acceder con vscode, escribir en el cmd: code .


1) Activar entorno virtual (ingresar al directorio del entorno)
	cd Scripts
	activate

2) Desactivar el entorno
	deactivate

3) Instalar Django
	pip install django

4) Validar version de Django
	python -m django --version
	pip list

5) rear un protyecto Django (asegurar que este en la carpeta raiz del entorno)
	django-admin startproject <nombre_proyecto> .

6) Levantar el servidor
	python .\manage.py runserver


7) Ejecutar migraciones
	python .\manage.py makemigrations
	python .\manage.py migrate



*************************************************************************

--------------------------------------------------------------------
- cambiar configuracion (archivo: proyecto\__pycache__\settings.py) -
--------------------------------------------------------------------
(cambiar idioma)
LANGUAGE_CODE = 'es'

(instalarar las app a utilizar)
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

(definir los middleware de seguridad)
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

(plantillas de paginas)
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

(conexion a base de datos)
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}

(proceso de login)
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


--------------------------------------------------------------------
- definir rutas (archivo: proyecto\__pycache__\urls.py) -
--------------------------------------------------------------------





